package edu.rit.cs.lemoine_conjecture;

/**
 * InputValidator class provides all input validation for command-line arguments.
 * This same InputValidator class can be re-used for multiple program implementations
 * that require identical command-line argument support.
 *
 * @author Eric Hartman
 * @version 12-Sep-2019
 */
public class InputValidator {

    public static int lowerBound = -1;
    public static int upperBound = -1;

    /**
     * validate method performs the command-line argument validation for
     * the given arguments.
     * @param args The list of command-line arguments.
     * @return returns True on validation success; false otherwise.
     */
    public static boolean validate(String[] args) {
        // Validate the arguments...
        if(args == null) {
            System.out.println("ERROR: no arguments were specified.");
            return false;
        }
        if(args.length != 2) {
            System.out.println("ERROR: incorrect number of arguments specified.");
            return false;
        }

        // Convert from String to int
        try {
            lowerBound = Integer.parseInt(args[0]);
            upperBound = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("ERROR: only integer arguments can be specified.");
            return false;
        }

        // Perform numeric input validations
        return validateNumericInputs(lowerBound, upperBound);
    }

    private static boolean validateNumericInputs(int lowerBound, int upperBound) {
        // Validate the lower bound:
        //   The lower bound must be an odd integer greater than 5
        if ((lowerBound <= 5) || (lowerBound % 2 == 0)) {
            System.out.println("ERROR: The lower bound must be an odd integer greater than 5.");
            return false;
        }

        // Validate the Upper bound:
        //   The Upper bound must be an odd integer greater than or equal to the lower bound
        if ((upperBound < lowerBound) || (upperBound % 2 == 0)) {
            System.out.println("ERROR: The upper bound must be an odd integer greater than or equal to the lower bound.");
            return false;
        }

        return true;
    }
}
