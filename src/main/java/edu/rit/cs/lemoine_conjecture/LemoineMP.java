package edu.rit.cs.lemoine_conjecture;

import java.util.ArrayList;
import java.util.List;
import edu.rit.cs.Prime;

/**
 * LemoineMP class implements a Parallel version of an algorithm
 * that seeks an optimal equation n = p * 2q by minimizing p and maximizing n
 * for an inclusive range of numbers n between a lowerBound and an upperBound.
 *
 * @author  Eric Hartman
 * @version 09-Sep-2019
 */
public class LemoineMP {
    private final int lowerBound;
    private final int upperBound;

    /**
     * LemoineMP constructor.  When constructing a LemoineMP object,
     * there are no calculations performed.  Calculations are deferred
     * until the calculateResult method is called, at which time
     * they are attempted to be performed in parallel leveraging
     * omp4j.
     *
     * @param lowerBound is the lower bound of the sequence, inclusive
     * @param upperBound is the upper bound of the sequence, inclusive
     */
    public LemoineMP(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    /**
     * calculateCandidate method performs all of the work to determine
     * the optimal n,p,q values of the n = p + 2*q equation for a
     * given n whereby p is minimized.
     *
     * @param n the integer value of n for finding the optimal p and q values
     * @return Returns an NPQ structure containing the optimal n,p,q values.
     */
    private NPQ calculateCandidate(int n) {
        NPQ candidate = null;

        // n = p + 2*q  (p is odd)
        // Iterate over all possible prime p's (from 1 to n-2)
        Prime.Iterator primeIterator = new Prime.Iterator();

        for(int p = primeIterator.next(); p < (n-2); p = primeIterator.next()) {
            //System.out.println("Iterating n=" + n + ", prime=" + p);
            // Calculate q: n=p+2q -> 2q = n-p -> q = (n-p)/2
            //   n-p must be even
            //   p < n due to our loop condition
            int delta = n - p;
            if (delta % 2 == 0) {
                int q =  delta / 2;
                //System.out.println("Iterating n=" + n + ", prime=" + p + ", q=" + q);
                if (Prime.isPrime(q)) {
                    // We have found a candidate!  Keep it since p is guaranteed to be largest so far
                    // due to the order of our prime iterator from least to greatest p.
                    candidate = new NPQ(n,p,q);
                    //System.out.println("candidate: " + n + " = " + p + " + 2*" + q );
                    return candidate;
                }
            }
        }

        return candidate;
    }

    /**
     * findNPQWithLargestN method - given a list of NPQ objects, find the NPQ object
     * with the largest n value.
     *
     * @param list a list of NPQ objects
     * @return an NPQ object with the highest n value
     */
    private NPQ findNPQWithLargestN(List<NPQ> list) {
        int largestN = 0;
        NPQ winner = null;

        for(NPQ candidate : list) {
            if(candidate.n > largestN) {
                largestN = candidate.n;
                winner = candidate;
            }
        }

        return winner;
    }

    /**
     * calculateResult method performs the deferred calculations to determine
     * the optimal values of n,p,q for a given range on n values.
     * Uses omp4j pre-processor to parallelize the for loop which
     * includes a critical section to guard data access to the shared winners ArrayList.
     *
     * Once the optimal candidate is found, the result is printed to the console
     * as "n = p + 2*q"
     *
     * @return the optimal NPQ object; this formal result is needed for JUnit test simplification.
     */
    public NPQ calculateResult() {
        List<NPQ> winners = new ArrayList<NPQ>();

        // Let's iterate over the range of odd numbers
        // omp parallel for
        for(int n = lowerBound; n <= upperBound; n = n + 2) {
            //System.out.println("Processing n=" + n + "");
            NPQ candidate = calculateCandidate(n);

            // omp critical
            {
                // Merge candidate into winners appropriately...
                if (winners.size() == 0) {
                    // Empty, so candidate is de-facto winner so far!
                    winners.add(candidate);
                } else {
                    if (candidate.p > winners.get(0).p) {
                        // Compare p value: if candidate.p greater, this is only winner now
                        winners.clear();
                        winners.add(candidate);
                    } else if (candidate.p == winners.get(0).p) {
                        // Compare p value: if candidate.p equal, then add to winners AND choose winner as NPQ with largest n value
                        winners.add(candidate);
                        NPQ winner = findNPQWithLargestN(winners);
                        winners.clear();
                        winners.add(winner);
                    }
                }
            }
        }

        // Let's output our results
        NPQ result = winners.get(0);
        System.out.println("" + result.n + " = " + result.p + " + 2*" + result.q );
        return result;
    }

    /**
     * main() method, entry point for LemoineSeq.java
     *
     * @param args - Two integer arguments are required,
     *             First argument is the lower bound which must be an odd integer greater than 5
     *             Second argument is the upper bound which must be greater than or equal to the first
     *               argument and odd.
     */
    public static void main(String[] args) {
        if(InputValidator.validate(args)) {
            LemoineMP lemoine = new LemoineMP(InputValidator.lowerBound, InputValidator.upperBound);
            lemoine.calculateResult();
        }
    }
}
