package edu.rit.cs.lemoine_conjecture;

/**
 * NPQ class is a container for storage and transport of n,p,q triples.
 *
 *  @author  Eric Hartman
 *  @version 09-Sep-2019
 */
public class NPQ {
    public int n, p, q;

    /**
     * Constructor for an NPQ object that is intended for storing the
     * n,p,q variables in the equation n = p + 2*q.
     *
     * @param n an integer value
     * @param p an integer value
     * @param q an integer value
     */
    public NPQ (int n, int p, int q) {
        this.n = n;
        this.p = p;
        this.q = q;
    }
}
