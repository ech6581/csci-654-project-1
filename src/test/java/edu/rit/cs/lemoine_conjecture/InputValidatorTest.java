package edu.rit.cs.lemoine_conjecture;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * InputValidatorTest performs unit tests of the InputValidator class.
 *
 * @author Eric Hartman
 * @version 12-Sep-2019
 */
class InputValidatorTest {

    @org.junit.jupiter.api.Test
    /**
     * Performs unit tests of the validate method.
     */
    void validate() {
        assertFalse(InputValidator.validate(null),
                "validate(null) should return false");
        assertFalse(InputValidator.validate(new String[] {"one", "two"}),
                "validate('one','two') should return false");
        assertFalse(InputValidator.validate(new String[] {"1", "two"}),
                "validate('1','two') should return false");
        assertFalse(InputValidator.validate(new String[] {"1", "2"}),
                "validate('1','2') should return false");
        assertFalse(InputValidator.validate(new String[] {"6", "6"}),
                "validate('6','6') should return false");
        assertFalse(InputValidator.validate(new String[] {"6", "7"}),
                "validate('6','7') should return false");
        assertTrue(InputValidator.validate(new String[] {"7", "7"}),
                "validate('7','7') should return true");
        assertFalse(InputValidator.validate(new String[] {"7", "8"}),
                "validate('7','8') should return false");
        assertFalse(InputValidator.validate(new String[] {"9", "7"}),
                "validate('9','7') should return false");
        assertFalse(InputValidator.validate(new String[] {"7", "nine"}),
                "validate('7','nine') should return false");
        assertFalse(InputValidator.validate(new String[] {null, null}),
                "validate(null,null) should return false");
        assertFalse(InputValidator.validate(new String[] {"8", "8"}),
                "validate('8','8') should return false");
    }
}