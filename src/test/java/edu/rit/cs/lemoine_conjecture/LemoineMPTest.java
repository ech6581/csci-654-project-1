package edu.rit.cs.lemoine_conjecture;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * LemoineMPTest performs unit tests of the LemoineMP class.
 *
 * @author Eric Hartman
 * @version 12-Sep-2019
 */
class LemoineMPTest {

    /**
     * Performs unit tests of the calculateResult method.
     */
    @Test
    void calculateResult() {
        LemoineMP lemoineMP = null;
        NPQ result = null;

        // Provide tests to verify the outputs for the examples given in the requirements
        lemoineMP = new LemoineMP(7,7);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 7, "7 = 3 + 2*2");
        assertEquals(result.p, 3, "7 = 3 + 2*2");
        assertEquals(result.q, 2, "7 = 3 + 2*2");

        lemoineMP = new LemoineMP(9,9);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 9, "9 = 3 + 2*3");
        assertEquals(result.p, 3, "9 = 3 + 2*3");
        assertEquals(result.q, 3, "9 = 3 + 2*3");

        lemoineMP = new LemoineMP(11,11);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 11, "11 = 5 + 2*3");
        assertEquals(result.p, 5, "11 = 5 + 2*3");
        assertEquals(result.q, 3, "11 = 5 + 2*3");

        lemoineMP = new LemoineMP(13,13);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 13, "13 = 3 + 2*5");
        assertEquals(result.p, 3, "13 = 3 + 2*5");
        assertEquals(result.q, 5, "13 = 3 + 2*5");

        lemoineMP = new LemoineMP(15,15);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 15, "15 = 5 + 2*5");
        assertEquals(result.p, 5, "15 = 5 + 2*5");
        assertEquals(result.q, 5, "15 = 5 + 2*5");

        lemoineMP = new LemoineMP(17,17);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 17, "17 = 3 + 2*7");
        assertEquals(result.p, 3, "17 = 3 + 2*7");
        assertEquals(result.q, 7, "17 = 3 + 2*7");

        lemoineMP = new LemoineMP(19,19);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 19, "19 = 5 + 2*7");
        assertEquals(result.p, 5, "19 = 5 + 2*7");
        assertEquals(result.q, 7, "19 = 5 + 2*7");

        lemoineMP = new LemoineMP(99,99);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 99, "99 = 5 + 2*47");
        assertEquals(result.p, 5, "99 = 5 + 2*47");
        assertEquals(result.q, 47, "99 = 5 + 2*47");

        lemoineMP = new LemoineMP(199,199);
        result = lemoineMP.calculateResult();
        assertEquals(result.n, 199, "199 = 5 + 2*97");
        assertEquals(result.p, 5, "199 = 5 + 2*97");
        assertEquals(result.q, 97, "199 = 5 + 2*97");
    }
}