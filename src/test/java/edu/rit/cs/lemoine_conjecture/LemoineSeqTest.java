package edu.rit.cs.lemoine_conjecture;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * LemoineSeqTest performs unit tests of the LemoineMP class.
 *
 * @author Eric Hartman
 * @version 12-Sep-2019
 */
class LemoineSeqTest {

    /**
     * Performs unit tests of the calculateResult method.
     */
    @Test
    void calculateResult() {
        LemoineSeq lemoineSeq = null;
        NPQ result = null;

        // Provide tests to verify the outputs for the examples given in the requirements
        lemoineSeq = new LemoineSeq(7,7);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 7, "7 = 3 + 2*2");
        assertEquals(result.p, 3, "7 = 3 + 2*2");
        assertEquals(result.q, 2, "7 = 3 + 2*2");

        lemoineSeq = new LemoineSeq(9,9);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 9, "9 = 3 + 2*3");
        assertEquals(result.p, 3, "9 = 3 + 2*3");
        assertEquals(result.q, 3, "9 = 3 + 2*3");

        lemoineSeq = new LemoineSeq(11,11);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 11, "11 = 5 + 2*3");
        assertEquals(result.p, 5, "11 = 5 + 2*3");
        assertEquals(result.q, 3, "11 = 5 + 2*3");

        lemoineSeq = new LemoineSeq(13,13);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 13, "13 = 3 + 2*5");
        assertEquals(result.p, 3, "13 = 3 + 2*5");
        assertEquals(result.q, 5, "13 = 3 + 2*5");

        lemoineSeq = new LemoineSeq(15,15);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 15, "15 = 5 + 2*5");
        assertEquals(result.p, 5, "15 = 5 + 2*5");
        assertEquals(result.q, 5, "15 = 5 + 2*5");

        lemoineSeq = new LemoineSeq(17,17);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 17, "17 = 3 + 2*7");
        assertEquals(result.p, 3, "17 = 3 + 2*7");
        assertEquals(result.q, 7, "17 = 3 + 2*7");

        lemoineSeq = new LemoineSeq(19,19);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 19, "19 = 5 + 2*7");
        assertEquals(result.p, 5, "19 = 5 + 2*7");
        assertEquals(result.q, 7, "19 = 5 + 2*7");

        lemoineSeq = new LemoineSeq(99,99);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 99, "99 = 5 + 2*47");
        assertEquals(result.p, 5, "99 = 5 + 2*47");
        assertEquals(result.q, 47, "99 = 5 + 2*47");

        lemoineSeq = new LemoineSeq(199,199);
        result = lemoineSeq.calculateResult();
        assertEquals(result.n, 199, "199 = 5 + 2*97");
        assertEquals(result.p, 5, "199 = 5 + 2*97");
        assertEquals(result.q, 97, "199 = 5 + 2*97");
    }
}